//
//  TrackPlayerViewModel.swift
//  iTunesPlayer
//
//  Created by Santi Bernaldo on 28/06/2018.
//  Copyright © 2018 Santi Bernaldo. All rights reserved.
//

import SDWebImage

struct TrackPlayerViewModel {
    var urlThumbnail: String
    var nameArtist: String
    var nameTrack: String
    var durationTrack: String
    var timeElapsed: String
    var model: ResultModel?
    var allTracks: [ResultModel]?
    var viewController: TrackPlayerViewController?
    var indexTrackSelected: Int
    var previousTrack: ResultModel? {
        get {
            guard let tracks = allTracks else { return nil }
            let indexPrevious = indexTrackSelected-1
            guard let previousTrack = tracks[safe:indexPrevious] else { return nil }
            return previousTrack
        }
    }
    var nexTrack: ResultModel? {
        get {
            guard let tracks = allTracks else { return nil }
            let indexNext = indexTrackSelected+1
            guard let previousTrack = tracks[safe:indexNext] else { return nil }
            return previousTrack
        }
    }
    
    //From the API Documentation about the preview time
    /*
    30-second preview file for the content associated with the returned media type.
    */
    static let TrackPreviewTime : Float = 30.0
    
    mutating func configureViewModel() {
        guard let model = model else { return }
        
        urlThumbnail = model.artworkUrl100
        nameArtist = model.artistName
        nameTrack = model.trackName
        guard let duration = model.trackTimeMillis else { return }
        durationTrack = String(duration)
        PlayerManager.manager.urlString = model.previewUrl
    }
    
    func setupControllerLayout(_ viewController: TrackPlayerViewController) {
        viewController.imageThumbnail.sd_setImage(with: URL(string: urlThumbnail))
        viewController.labelNameArtist.text = nameArtist
        viewController.labelNameTrack.text = nameTrack
        viewController.labelNameDuration.text = durationTrack.timeFormatted()
    }
    
    mutating func configureAfterNewTrackSelected(newModel: ResultModel,
                                                 viewController: TrackPlayerViewController) {
        model = newModel
        configureViewModel()
        setupControllerLayout(viewController)
        PlayerManager.manager.urlString = newModel.previewUrl
        PlayerManager.manager.removeObserverWith(controller: viewController)
        PlayerManager.manager.preparePlayerWithObserver(controller: viewController)
    }
}

extension Collection {
    /// Returns the element at the specified index iff it is within bounds, otherwise nil.
    subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}

