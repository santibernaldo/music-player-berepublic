//
//  TrackPlayerViewController.swift
//  iTunesPlayer
//
//  Created by Santi Bernaldo on 28/06/2018.
//  Copyright © 2018 Santi Bernaldo. All rights reserved.
//

import UIKit
import SVProgressHUD
import AVFoundation

class TrackPlayerViewController:  UIViewController {
    var viewModel = TrackPlayerViewModel(urlThumbnail: "", nameArtist: "", nameTrack: "", durationTrack: "", timeElapsed: "", model: nil, allTracks: nil, viewController: nil, indexTrackSelected: 0)
    
    @IBOutlet weak var imageThumbnail: UIImageView!
    
    @IBOutlet weak var labelNameArtist: UILabel!
    
    @IBOutlet weak var labelNameTrack: UILabel!
    
    @IBOutlet weak var labelNameDuration: UILabel!
    
    @IBOutlet weak var playButton: UIButton!
    
    @IBOutlet weak var stopButton: UIButton!
    
    @IBOutlet weak var slider: UISlider!
    
    @objc var player: AVPlayer {
        get {
            return PlayerManager.manager.player
        }
    }
    
    struct Constants {
        static let PlayImageName = "play"
        static let PauseImageName = "pause"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        edgesForExtendedLayout = []
        title = "Reproductor"
        
        configuration()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        PlayerManager.manager.preparePlayerWithObserver(controller: self)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        stopAction()
        PlayerManager.manager.removeObserverWith(controller: self )
    }
    
    private func configuration() {
        SVProgressHUD.show()
        viewModel.setupControllerLayout(self)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            SVProgressHUD.dismiss()
        }
    }
    
    @IBAction func shareTrack(_ sender: Any) {
        let textToShare = "Comparte la canción en tus redes sociales"
                
        guard let artistViewUrl = viewModel.model?.trackViewUrl else { return }
        guard let url = URL(string: artistViewUrl) else { return }
        
        let objectsToShare: [Any] = [textToShare, url]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        
        present(activityVC, animated: true, completion: nil)
    }
}

//Actions
extension TrackPlayerViewController {
    @IBAction func play(_ sender: Any)  {
        if player.currentItem == nil {
            PlayerManager.manager.preparePlayerWithObserver(controller: self)
        }

        switch player.timeControlStatus {
        case .playing:
            playButton.setImage(UIImage(named:Constants.PlayImageName), for: .normal)
            player.pause()
        case .paused:
            playButton.setImage(UIImage(named:Constants.PlayImageName), for: .normal)
            playAction()
        case .waitingToPlayAtSpecifiedRate:
            playButton.setImage(UIImage(named:Constants.PauseImageName), for: .normal)
            playAction()
        }
    }
    
  
    @IBAction func stop(_ sender: Any) {
       stopAction()
    }
    
    fileprivate func playAction() {
        SVProgressHUD.show()
        player.play()
    }
    
    @IBAction func previous(_ sender: Any) {
        guard let previousTrack = viewModel.previousTrack else { return }
        viewModel.indexTrackSelected -= 1
        viewModel.configureAfterNewTrackSelected(newModel: previousTrack, viewController: self)
        playAction()
    }
    
    @IBAction func next(_ sender: Any) {
        guard let nextTrack = viewModel.nexTrack else { return }
        viewModel.indexTrackSelected += 1
        viewModel.configureAfterNewTrackSelected(newModel: nextTrack, viewController: self)
        playAction()
    }
  
    fileprivate func stopAction() {
        PlayerManager.manager.stop()
        playButton.setImage(UIImage(named:Constants.PlayImageName), for: .normal)
        self.slider.value = 0.0
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == #keyPath(player.timeControlStatus) {
            if player.timeControlStatus == .playing {
                playButton.setImage(UIImage(named:"pause"), for: .normal)
                SVProgressHUD.dismiss()

                let _ = player.addPeriodicTimeObserver(forInterval: CMTime(seconds: 1, preferredTimescale: CMTimeScale(NSEC_PER_SEC)), queue: DispatchQueue.main) { [weak self] (time) in
                    if let _ = self?.player.currentItem {
                        self?.slider.value = Float(CMTimeGetSeconds(time)) / TrackPlayerViewModel.TrackPreviewTime
                    }
                }
            }
        }
    }
}

