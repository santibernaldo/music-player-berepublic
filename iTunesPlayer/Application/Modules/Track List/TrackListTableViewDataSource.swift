//
//  TrackListTableViewDataSource.swift
//  iTunesPlayer
//
//  Created by Santi Bernaldo on 28/06/2018.
//  Copyright © 2018 Santi Bernaldo. All rights reserved.
//

import UIKit

protocol TableViewDataSourceProtocol {
    func setupDataSource(tableView: UITableView)
}

class TrackListTableViewDataSource: NSObject, TableViewDataSourceProtocol {
 
    fileprivate var tableView: UITableView = UITableView()
    
    var viewModel : TrackListViewModel
    
    fileprivate var viewModelCell = TrackListCellViewModel()
    
    func setupDataSource(tableView: UITableView) {
        tableView.register(
            UINib(nibName: TrackListCellViewModel.CellName, bundle: nil),
            forCellReuseIdentifier: TrackListCellViewModel.TrackListCellIdentifier
        )
        tableView.dataSource = self
    }
    
    init(viewModel: TrackListViewModel) {
        self.viewModel = viewModel
    }
}

extension TrackListTableViewDataSource: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let results = viewModel.model?.results else { return 0 }
        return results.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard var results = viewModel.model?.results else { return UITableViewCell() }
        results = (viewModel.resultsFiltered != nil) ? viewModel.resultsFiltered! : results

        let cell = tableView.dequeueReusableCell(withIdentifier: TrackListCellViewModel.TrackListCellIdentifier, for: indexPath) as! TrackListCell
        
        viewModelCell.configure(
            result: results[indexPath.row],
            cell: cell
        )
        
        return cell
    }
}
