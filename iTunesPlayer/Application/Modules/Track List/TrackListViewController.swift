//
//  ViewController.swift
//  iTunesPlayer
//
//  Created by Santi Bernaldo on 26/06/2018.
//  Copyright © 2018 Santi Bernaldo. All rights reserved.
//

import UIKit
import PromiseKit
import SVProgressHUD

class TrackListViewController: UIViewController {

    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var tableView: UITableView!
    
    var viewModel = TrackListViewModel()
    
    fileprivate var tableViewProtocols: (delegate: TrackListTableViewDelegate,
    dataSource: TrackListTableViewDataSource)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupLayout()
    }

    func getSearchResult(searchBarText: String) {
        viewModel.params = [TrackListViewModel.ParamQueryConstant : searchBarText]
        
        SVProgressHUD.show()
        viewModel.getSearchModel(query: viewModel.params) { (searchModel) in
            
            SVProgressHUD.dismiss()
            
            if let searchModel = searchModel {
                self.viewModel.model = searchModel
                self.viewModel.resultsFiltered = nil
                self.configureTableViewProtocols(self.viewModel)
                self.segmentedControl.isEnabled = true
                
                //Reload data
                self.tableView.reloadData()
            } else {
                SVProgressHUD.showError(withStatus: "Error encontrado.")
            }
        }
    }
    
    func configureTableViewProtocols(_ viewModel: TrackListViewModel){
        self.tableViewProtocols = (
            TrackListTableViewDelegate(
                viewModel: viewModel,
                viewController:self
            ),
            TrackListTableViewDataSource(viewModel: viewModel)
        )
        
        self.tableViewProtocols?.dataSource.setupDataSource(tableView: tableView)
        self.tableViewProtocols?.delegate.setupDelegate(tableView: tableView)
    }
}

extension TrackListViewController {
    //MARK: Configuration
    fileprivate func setupLayout() {
        edgesForExtendedLayout = []
        
        navigationController?.navigationBar.prefersLargeTitles = true
        title = "Buscador"
    
        setupSegmentedControl(segmentedControl)
        
        if let navigationController = navigationController {
            navigationController.navigationBar.backgroundColor = .white
        }
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = CGFloat(151.0)
        
        segmentedControl.isEnabled = false
    }
    
    fileprivate func setupSegmentedControl(_ segmentedControl: UISegmentedControl) {
        segmentedControl.removeAllSegments()
        for (index, filterType) in viewModel.arrayFiltersType.enumerated() {
            segmentedControl.insertSegment(withTitle: filterType, at: index, animated: false)
        }
    }
    
    //MARK: Actions
    @IBAction func segmentedValueChanged(_ sender: UISegmentedControl) {
        guard let filter = TrackListViewModel.FilterType(rawValue: sender.selectedSegmentIndex) else { return }
        
        SVProgressHUD.show()
        viewModel.filterApplied = filter
        
        guard let resultsFiltered = viewModel.getArrayResultsFiltered() else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                SVProgressHUD.dismiss()
            }
            return
        }
        tableViewProtocols?.dataSource.viewModel.resultsFiltered = resultsFiltered
        tableViewProtocols?.delegate.viewModel.resultsFiltered = resultsFiltered
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            SVProgressHUD.dismiss()
        }
        tableView.reloadData()
    }
}

extension TrackListViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let text = searchBar.text else { return }
        getSearchResult(searchBarText: text)
    }
}

