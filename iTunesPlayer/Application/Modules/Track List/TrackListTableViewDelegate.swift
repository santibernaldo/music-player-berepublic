//
//  TrackListTableViewDelegate.swift
//  iTunesPlayer
//
//  Created by Santi Bernaldo on 28/06/2018.
//  Copyright © 2018 Santi Bernaldo. All rights reserved.
//

import UIKit

protocol TableViewDelegateProtocol {
    func setupDelegate(tableView: UITableView)
}

class TrackListTableViewDelegate: NSObject, TableViewDelegateProtocol {
    
    fileprivate var tableView: UITableView = UITableView()
    
    var viewModel : TrackListViewModel
    
    fileprivate var viewController: TrackListViewController
    
    func setupDelegate(tableView: UITableView) {
        tableView.delegate = self
    }
    
    init(viewModel: TrackListViewModel,
         viewController: TrackListViewController) {
        self.viewModel = viewModel
        self.viewController = viewController
    }
}

extension TrackListTableViewDelegate: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard var results = viewModel.model?.results else { return }
        results = (viewModel.resultsFiltered != nil) ? viewModel.resultsFiltered! : results
        
        let result = results[indexPath.row]
        let controllerPlayer = TrackPlayerViewController()
        controllerPlayer.viewModel.model = result
        controllerPlayer.viewModel.configureViewModel()
        controllerPlayer.viewModel.allTracks = results
        controllerPlayer.viewModel.indexTrackSelected = indexPath.row
        
        viewController.navigationController?.pushViewController(controllerPlayer, animated: true)
    }
}
