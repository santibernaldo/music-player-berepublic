//
//  TrackListInteractor.swift
//  iTunesPlayer
//
//  Created by Santi Bernaldo on 28/06/2018.
//  Copyright © 2018 Santi Bernaldo. All rights reserved.
//

import Foundation

struct TrackListInteractor {
    
    static let sharedInstance = TrackListInteractor()
    
    func getSearchResults(query: [String:Any], completionBlock: @escaping ((SearchModel?) -> Void)){
        APIClient.provider.performSearchWith(parameters: query).done { searchModel in
            completionBlock(searchModel)
        }.catch { error in
             completionBlock(nil)
        }
    }
}

