//
//  TrackListViewModel.swift
//  iTunesPlayer
//
//  Created by Santi Bernaldo on 28/06/2018.
//  Copyright © 2018 Santi Bernaldo. All rights reserved.
//

import UIKit
import SDWebImage

struct TrackListCellViewModel {
    var nameTrack: String
    var nameArtist: String
    var nameAlbum: String
    var dateRelease: String
    var urlImageThumbnail: String
    var durationString: String
    var genre: String
    var price: Float
    
    static let SizeCell: CGFloat = CGFloat(90.0)
    static let TrackListCellIdentifier = "TrackListCellIdentifier"
    static let CellName = "TrackListCell"
    static let CellSize = CGFloat(100.0)
    
    mutating func configure(result: ResultModel, cell: TrackListCell) {
       nameTrack = result.trackName
       nameArtist = result.artistName
       nameAlbum = result.collectionName ?? ""
       dateRelease = result.releaseDate ?? ""
       urlImageThumbnail = result.artworkUrl100
       if let duration = result.trackTimeMillis {
          durationString = String(duration)
       }
       genre = result.primaryGenreName ?? ""
       price = result.trackPrice ?? 0.0
        
       configureLayoutFor(cell)
    }
    
    func configureLayoutFor(_ cell: TrackListCell) {
        cell.labelTrackName.text = nameTrack
        cell.labelArtistName.text = nameArtist
        cell.labelArtistName.sizeToFit()
        cell.labelAlbumName.text = nameAlbum
        cell.labelReleaseDate.text = dateRelease.dateFormatted()
        cell.labelReleaseDate.sizeToFit()
        cell.labelGenre.text = genre
        cell.imageThumbnail.sd_setImage(with: URL(string: urlImageThumbnail))
        cell.labelPrice.text = String(price) + " $"
        cell.labelDuration.text = durationString.timeFormatted()
    }
    
    init() {
        nameTrack = ""
        nameArtist = ""
        nameAlbum = ""
        dateRelease = ""
        urlImageThumbnail = ""
        durationString = ""
        genre = ""
        price = 0.0
    }
}
