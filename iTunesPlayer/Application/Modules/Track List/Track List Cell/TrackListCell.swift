//
//  TrackListCell.swift
//  iTunesPlayer
//
//  Created by Santi Bernaldo on 28/06/2018.
//  Copyright © 2018 Santi Bernaldo. All rights reserved.
//

import UIKit

class TrackListCell : UITableViewCell {
    @IBOutlet weak var imageThumbnail: UIImageView!
    @IBOutlet weak var labelArtistName: UILabel!
    @IBOutlet weak var labelTrackName: UILabel!
    @IBOutlet weak var labelGenre: UILabel!
    @IBOutlet weak var labelDuration: UILabel!
    @IBOutlet weak var labelReleaseDate: UILabel!
    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var labelAlbumName: UILabel!
}

