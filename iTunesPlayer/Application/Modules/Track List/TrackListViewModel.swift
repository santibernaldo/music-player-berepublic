//
//  TrackListViewModel.swift
//  iTunesPlayer
//
//  Created by Santi Bernaldo on 28/06/2018.
//  Copyright © 2018 Santi Bernaldo. All rights reserved.
//

import UIKit

struct TrackListViewModel {
    var params: [String:Any]
    var seachBarText: String
    var filterApplied : FilterType
    let arrayFiltersType = ["Duración", "Género", "Precio"]
    var model: SearchModel?
    var resultsFiltered: [ResultModel]?
    
    static let ParamQueryConstant : String = "term"
    static let ViewControllerName : String = "TrackListViewController"
    
    enum FilterType: Int {
        case Duration = 0
        case Genre = 1
        case Price = 2
        case NoFilter
    }
    
    init() {
        params = ["":""]
        seachBarText = ""
        filterApplied = .NoFilter
        model = nil
        resultsFiltered = nil
    }
    
    mutating func getSearchModel(query: [String:Any], onCompletion: @escaping ((SearchModel?) -> Void)) {
        TrackListInteractor.sharedInstance.getSearchResults(query: query) { (searchModel) in
            if let searchModel = searchModel {
                onCompletion(searchModel)
            } else {
                onCompletion(nil)
            }
        }
    }
    
    func getArrayResultsFiltered() -> [ResultModel]? {
        guard let results = model?.results else { return nil }
        var arrayFiltered = [ResultModel]()
        
        switch filterApplied {
        case .Duration:
            arrayFiltered =  results.sorted {
                guard let durationOne = $0.trackTimeMillis, let durationTwo = $1.trackTimeMillis else { return false }
                return durationOne < durationTwo
            }
        case .Genre:
            arrayFiltered =  results.sorted {
                guard let durationOne = $0.primaryGenreName, let durationTwo = $1.primaryGenreName else { return false }
                return durationOne < durationTwo
            }
        case .Price:
            arrayFiltered =  results.sorted {
                guard let durationOne = $0.trackPrice, let durationTwo = $1.trackPrice else { return false }
                return durationOne < durationTwo
            }
        default:
            print("")
        }
        
        return arrayFiltered
    }
}
