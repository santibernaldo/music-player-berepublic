//
//  SearchEndPoint.swift
//  iTunesPlayer
//
//  Created by Santi Bernaldo on 26/06/2018.
//  Copyright © 2018 Santi Bernaldo. All rights reserved.
//

import Alamofire

enum APIRouter: URLRequestConvertible {
    static let baseURLString = "https://itunes.apple.com"
    
    case searchWith(parameters: [String:Any])
    
    var method: Alamofire.HTTPMethod {
        switch self {
        case .searchWith:
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .searchWith:
            return "/search"
        }
    }

    func asURLRequest() throws -> URLRequest {
        let url = try APIRouter.baseURLString.asURL()
       
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        
        switch self {
        case .searchWith(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        }
        
        return urlRequest
    }
}

