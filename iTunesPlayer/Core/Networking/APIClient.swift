//
//  ApiClient.swift
//  º
//
//  Created by Santi Bernaldo on 26/06/2018.
//  Copyright © 2018 Santi Bernaldo. All rights reserved.
//

import Alamofire
import PromiseKit

class APIClient {
    
    static let provider = APIClient()

    func performSearchWith(parameters: [String:Any]) -> Promise<SearchModel> {
        return Alamofire.request(APIRouter.searchWith(parameters: parameters))
            .responseCodable()
    }
}

extension Alamofire.DataRequest {
    public func responseCodable<T: Codable>() -> Promise<T> {
        return Promise { seal in
            responseData(queue: nil) { response in
                switch response.result {
                case .success(let value):
                    let decoder = JSONDecoder()
                    do {
                        seal.fulfill(try decoder.decode(T.self, from: value))
                    } catch let e {
                        seal.reject(e)
                    }
                case .failure(let error):
                    seal.reject(error)
                }
            }
        }
    }
}

