//
//  TimeFormatted.swift
//  iTunesPlayer
//
//  Created by Santi Bernaldo on 01/07/2018.
//  Copyright © 2018 Santi Bernaldo. All rights reserved.
//

import Foundation

extension String {
    func timeFormatted() -> String {
        guard var totalSeconds = Int(self) else { return "" }
        
        totalSeconds /= 1000
        let seconds = totalSeconds % 60
        let minutes = (totalSeconds / 60) % 60
        let hours = totalSeconds / 3600
        let strHours = hours > 9 ? String(hours) : String(hours)
        let strMinutes = minutes > 9 ? String(minutes) : String(minutes)
        let strSeconds = seconds > 9 ? String(seconds) : "0" + String(seconds)
        
        if hours > 0 {
            return "\(strHours):\(strMinutes):\(strSeconds)"
        }
        else {
            return "\(strMinutes):\(strSeconds)"
        }
    }
    
    func dateFormatted() -> String {
        let dateFormatter = ISO8601DateFormatter()
        dateFormatter.formatOptions = [.withYear, .withMonth, .withDay, .withDashSeparatorInDate]
        
        guard let date = dateFormatter.date(from: self) else { return "" }
        return dateFormatter.string(from: date)
    }
}
