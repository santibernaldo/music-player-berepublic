//
//  PlayerManager.swift
//  iTunesPlayer
//
//  Created by Santi Bernaldo on 01/07/2018.
//  Copyright © 2018 Santi Bernaldo. All rights reserved.
//

import Foundation
import AVFoundation

class PlayerManager: NSObject {
    
   @objc static let manager = PlayerManager()
    
   @objc var player: AVPlayer = AVPlayer(playerItem: nil)
    
    var urlString = String()
    var urlPreview : URL? {
        get {
            return URL(string: self.urlString)
        }
    }
    
    func preparePlayerWithObserver(controller: TrackPlayerViewController) {
        guard let url = urlPreview else { return }
        let playerItem = AVPlayerItem(url: url)
        player = AVPlayer(playerItem: playerItem)
        
        addObserver(
            controller,
            forKeyPath: #keyPath(player.timeControlStatus),
            options: [.old, .new],
            context: nil
        )
    }
    
    func removeObserverWith(controller: TrackPlayerViewController) {
        removeObserver(controller, forKeyPath: #keyPath(player.timeControlStatus))
    }
    
    func play() {
        player.play()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(playerDidFinishPlaying),
            name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
            object: player.currentItem
        )
    }
    
    func stop() {
        player.pause()
        player.replaceCurrentItem(with: nil)
    }
    
    @objc func playerDidFinishPlaying() {
        stop()
    }
}
