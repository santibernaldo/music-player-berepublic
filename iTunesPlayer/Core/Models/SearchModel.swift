//
//  SearchModel.swift
//  iTunesPlayer
//
//  Created by Santi Bernaldo on 26/06/2018.
//  Copyright © 2018 Santi Bernaldo. All rights reserved.
//

import Foundation

struct SearchModel: Codable {
    let resultCount: Int
    var results: [ResultModel]?
}

struct ResultModel: Codable {
    let artistId: Int?
    let artistName: String
    let artistViewUrl: String?
    let artworkUrl100: String
    let artworkUrl30: String?
    let artworkUrl60: String?
    let collectionCensoredName: String?
    let collectionExplicitness: String?
    let collectionId: Int?
    let collectionName: String?
    let collectionPrice: Float
    let collectionViewUrl: String?
    let country: String?
    let currency: String?
    let discCount: Int?
    let discNumber: Int?
    let isStreamable: Bool?
    let kind: String?
    let previewUrl: String
    let primaryGenreName: String?
    let releaseDate: String?
    let trackCensoredName: String
    let trackCount: Int?
    let trackExplicitiness: String?
    let trackId: Int?
    let trackName: String
    let trackNumber: Int?
    let trackPrice: Float?
    let trackTimeMillis: Int?
    let trackViewUrl: String?
    let wrapperType: String?
}
